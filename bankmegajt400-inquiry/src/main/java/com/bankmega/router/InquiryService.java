package com.bankmega.router;

public interface InquiryService {
	public InquiryList findAll();
	public Inquiry find(int id);
	public void create(Inquiry inquiry);
	public void remove(int id);
}
