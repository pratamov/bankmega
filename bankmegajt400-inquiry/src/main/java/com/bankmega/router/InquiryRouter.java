package com.bankmega.router;
import org.apache.camel.builder.RouteBuilder;

public class InquiryRouter extends RouteBuilder {

	public static final String SERVICE_ADDRESS = "http://localhost:9292/bankmega/inquiry";

	@Override
	public void configure() throws Exception {

		from("cxfEndpoint").recipientList().simple("direct:${header.operationName}");

		from("direct:findAll")
			.setBody().simple("SELECT id, data FROM inquiry")
			.to("jdbc:mockDS")
			.log("${body}")
			.process(new FindallInquiryProcessor());

		from("direct:find")
			.setBody()
			.simple("SELECT id, data FROM inquiry WHERE id = ${body}")
			.to("jdbc:mockDS")
			.log("${body}")
			.process(new FindInquiryProcessor());

		from("direct:create")
			.process(new CreateInquiryProcessor())
			.setBody()
			.simple("INSERT INTO inquiry (data) VALUES ('${body}')")
			.to("jdbc:mockDS");

		from("direct:remove")
			.setBody()
			.simple("DELETE FROM inquiry WHERE id = ${body}")
			.to("jdbc:mockDS");

	}

}
