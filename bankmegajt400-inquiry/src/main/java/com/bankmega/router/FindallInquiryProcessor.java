package com.bankmega.router;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class FindallInquiryProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		
		List<HashMap<String, Object>> data = exchange.getIn().getBody(List.class);
		List<Inquiry> inquiries = new ArrayList<Inquiry>();
		for (HashMap<String, Object> row : data) {
			Inquiry inquiry = new Inquiry();
			inquiry.setId((int)row.get("id"));
			inquiry.setData((String) row.get("data"));
			inquiries.add(inquiry);
		}
		InquiryList list = new InquiryList();
		list.setInquiryList(inquiries);
		exchange.getOut().setBody(list);
		
	}

}
