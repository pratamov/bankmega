package com.bankmega.router;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CreateInquiryProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		Inquiry inquiry = exchange.getIn().getBody(Inquiry.class);
		exchange.getOut().setBody(inquiry.getData());
	}

}
