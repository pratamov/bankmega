package com.bankmega.router;

import java.util.List;

public class InquiryList {
	private List<Inquiry> inquiryList;

	public List<Inquiry> getInquiryList() {
		return inquiryList;
	}

	public void setInquiryList(List<Inquiry> inquiryList) {
		this.inquiryList = inquiryList;
	}
}
