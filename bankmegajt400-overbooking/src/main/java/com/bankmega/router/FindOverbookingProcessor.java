package com.bankmega.router;

import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class FindOverbookingProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		
		List<HashMap<String, Object>> data = exchange.getIn().getBody(List.class);
		if (data.size() > 0) {
			Overbooking overbooking = new Overbooking();
			overbooking.setId((int)data.get(0).get("id"));
			overbooking.setData((String)data.get(0).get("data"));
			exchange.getOut().setBody(overbooking);
		}
		
	}

}
