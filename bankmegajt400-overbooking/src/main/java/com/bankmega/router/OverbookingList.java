package com.bankmega.router;

import java.util.List;

public class OverbookingList {
	private List<Overbooking> overbookingList;

	public List<Overbooking> getOverbookingList() {
		return overbookingList;
	}

	public void setOverbookingList(List<Overbooking> overbookingList) {
		this.overbookingList = overbookingList;
	}
	
	
}
