package com.bankmega.router;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CreateOverbookingProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		Overbooking overbooking = exchange.getIn().getBody(Overbooking.class);
		exchange.getOut().setBody(overbooking.getData());
	}

}
