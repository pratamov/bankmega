package com.bankmega.router;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class FindallOverbookingProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		
		List<HashMap<String, Object>> data = exchange.getIn().getBody(List.class);
		List<Overbooking> overbookings = new ArrayList<Overbooking>();
		for (HashMap<String, Object> row : data) {
			Overbooking overbooking = new Overbooking();
			overbooking.setId((int)row.get("id"));
			overbooking.setData((String) row.get("data"));
			overbookings.add(overbooking);
		}
		OverbookingList list = new OverbookingList();
		list.setOverbookingList(overbookings);
		exchange.getOut().setBody(list);
		
	}

}
