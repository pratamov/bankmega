package com.bankmega.router;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;

public class OverbookingRouter extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		restConfiguration().component("jetty")
			.bindingMode(RestBindingMode.json)
			.dataFormatProperty("prettyPrint", "true")
			.contextPath("/bankmega").port(8080)
			.apiContextPath("/overbooking-doc")
			.apiProperty("cors", "true");
		
		rest("/overbooking")
			.consumes("application/json").produces("application/json")
			.get().outType(OverbookingList.class)
				.to("direct:findAll")
			.get("/{id}").outType(Overbooking.class)
				.param().name("id").type(RestParamType.path).endParam()
				.to("direct:find")
			.post().type(Overbooking.class)
				.param().name("body").type(RestParamType.body).endParam()
				.to("direct:create")
			.delete("/{id}")
				.param().name("id").type(RestParamType.path).endParam()
				.to("direct:remove");

		from("direct:findAll")
			.setBody().simple("SELECT id, data FROM overbooking")
			.to("jdbc:mockDS")
			.log("${body}")
			.process(new FindallOverbookingProcessor());

		from("direct:find")
			.setBody()
			.simple("SELECT id, data FROM overbooking WHERE id = ${header.id}")
			.to("jdbc:mockDS")
			.log("${body}")
			.process(new FindOverbookingProcessor());

		from("direct:create")
			.process(new CreateOverbookingProcessor())
			.setBody()
			.simple("INSERT INTO overbooking (data) VALUES ('${body}')")
			.to("jdbc:mockDS");

		from("direct:remove")
			.setBody()
			.simple("DELETE FROM overbooking WHERE id = ${header.id}")
			.to("jdbc:mockDS");

	}

}
