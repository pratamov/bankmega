# Bank Mega Fuse Preparation

## Install Required Components

```
osgi:install -s wrap:mvn:commons-dbcp/commons-dbcp/1.4
osgi:install -s wrap:mvn:mysql/mysql-connector-java/5.1.45
osgi:install -s camel-jetty9
osgi:install -s camel-jetty-common
osgi:install -s camel-servletlistener
osgi:install -s camel-servlet
osgi:install -s camel-http-common
osgi:install -s camel-jt400
osgi:install -s camel-swagger
osgi:install -s camel-swagger-java
osgi:install -s camel-jackson
```

## bankmegajt400-inquiry (SOAP)

Install:
```
osgi:install -s mvn:com.bankmega/bankmega-inquiry/1.0.0
```

Endpoint (wsdl): http://localhost:9292/bankmega/inquiry?wsdl


## bankmegajt400-overbooking (REST)

Install:
```
osgi:install -s mvn:com.bankmega/bankmegajt400-overbooking/0.0.1
```

Endpoint (Swagger docs): http://localhost:8080/bankmega/overbooking-doc